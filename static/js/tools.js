/*
 * Cross Site Request Forgery protection
 * extraído de: https://docs.djangoproject.com/en/1.8/ref/csrf/
 *
 */

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

var csrftoken = getCookie('csrftoken');

/*
 * funciones personalizadas
 *
 */
function crearObjetoTransport(url, crear, actualizar, borrar) {
    var transport = {
        parameterMap: function(model, operation) {
            if (operation !== "read" && model) {
                return kendo.stringify(model);
            }
        },
        read: url
    };
    if (crear) {
        transport.create = {
            url: url,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }
    };
    if (actualizar) {
        transport.update = {
            url: function(modelo) {
                return url + modelo.id;
            },
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }
    };
    if (borrar) {
        transport.destroy = {
            url: function(modelo) {
                return url + modelo.id;
            },
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }
    };

    return transport
}
