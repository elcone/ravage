var Contacto = kendo.data.Model.define({
    id: "id",
    fields: {
        apellidos: { type: "string" },
        nombre: { type: "string" },
        direccion: { type: "string" },
        telefono: { type: "string" },
        fecha_de_nacimiento: { type: "date" },
        sexo: { type: "string" }
    }
});