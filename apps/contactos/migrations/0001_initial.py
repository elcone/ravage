# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cita',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('inicia', models.DateTimeField()),
                ('termina', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Contacto',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=50)),
                ('apellidos', models.CharField(max_length=50)),
                ('direccion', models.TextField(blank=True, null=True)),
                ('telefono', models.CharField(blank=True, null=True, max_length=20)),
                ('fecha_de_nacimiento', models.DateField(blank=True, null=True)),
                ('propietario', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='contactos')),
            ],
        ),
        migrations.AddField(
            model_name='cita',
            name='contacto',
            field=models.ForeignKey(to='contactos.Contacto', related_name='citas'),
        ),
        migrations.AddField(
            model_name='cita',
            name='propietario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='citas'),
        ),
    ]
