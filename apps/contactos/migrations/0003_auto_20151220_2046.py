# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contactos', '0002_contacto_sexo'),
    ]

    operations = [
        migrations.CreateModel(
            name='Nota',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('contenido', models.TextField()),
            ],
        ),
        migrations.AlterField(
            model_name='cita',
            name='propietario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, null=True, related_name='citas'),
        ),
        migrations.AlterField(
            model_name='contacto',
            name='propietario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, null=True, related_name='contactos'),
        ),
        migrations.AddField(
            model_name='nota',
            name='contacto',
            field=models.ForeignKey(to='contactos.Contacto', blank=True, null=True, related_name='notas'),
        ),
        migrations.AddField(
            model_name='nota',
            name='propietario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, null=True, related_name='notas'),
        ),
    ]
