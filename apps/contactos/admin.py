from django.contrib import admin
from .models import *

@admin.register(Contacto)
class ContactoAdmin(admin.ModelAdmin):
    list_display = [
        'apellidos',
        'nombre',
        'telefono',
        'fecha_de_nacimiento',
        'sexo',
        'propietario'
    ]
    list_filter = [
        'sexo'
    ]


@admin.register(Cita)
class CitaAdmin(admin.ModelAdmin):
    list_display = [
        'titulo',
        'inicia',
        'termina'
    ]


@admin.register(Nota)
class NotaAdmin(admin.ModelAdmin):
    pass
