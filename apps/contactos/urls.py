from django.conf.urls import url, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'contactos', ContactoViewSet)
router.register(r'citas', CitaViewSet)

urlpatterns = [
    url(r'api/', include(router.urls)),
    url(r'v2/', Contactos.as_view(), name='contactos'),
    url(r'^$', Inicio.as_view(), name='inicio')
]
