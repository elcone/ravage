from django.views.generic import TemplateView
from rest_framework.viewsets import ModelViewSet
from .models import *
from .serializers import *

class ContactoViewSet(ModelViewSet):
    serializer_class = ContactoSerializer
    queryset = Contacto.objects.all()

    def perform_create(self, serializer):
        serializer.save(propietario=self.request.user)


class CitaViewSet(ModelViewSet):
    serializer_class = CitaSerializer
    queryset = Cita.objects.all()

    def perform_create(self, serializer):
        serializer.save(propietario=self.request.user)


class Inicio(TemplateView):
    template_name = 'inicio.html'


class Contactos(TemplateView):
    template_name = 'contactos.html'
