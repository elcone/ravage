from rest_framework import serializers
from .models import *

class ContactoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contacto
        #fields = ('id', 'nombre_completo', 'apellidos', 'nombre')


class CitaSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='titulo')
    start = serializers.DateTimeField(source='inicia')
    end = serializers.DateTimeField(source='termina')

    class Meta:
        model = Cita
        fields = (
            'id',
            'title',
            'start',
            'end'
        )
